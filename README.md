# @universis/messages

Universis api server extension for implementing messaging services like sms messaging etc.

## Installation

        npm i @universis/messages

## Configuration

Register `SmsService` abstract service in application services by using a service strategy:

```
...
{
  "serviceType": "@universis/messages#SmsService",
  "strategyType": "./services/my-service#MySmsService"
}
...
```

Register also `SmsSchemaLoader` in application schema loaders:

```
"settings": {
    "schema": {
        "loaders": [
            ...
            {
                "loaderType": "@universis/messages#SmsSchemaLoader"
            }
        ]
    }
}
```

to activate the extensions of `@universis/messages` module.

## Development 

[`@universis/sms-gateway-catcher`](https://gitlab.com/universis/sms-gateway-catcher) simulates sms services for development purposes.

Start sms-gateway-catche by executing:

        npx @universis/sms-gateway-catcher

which is available by navigating to http://localhost:3030

Finally configure application services to use sms gateway catcher:

```
"services": [
    ...
    {
       "serviceType": "@universis/messages#SmsService",
       "strategyType": "./services/sms-gateway-catcher-service#SmsGatewayCatcherService"
    }
]
```

