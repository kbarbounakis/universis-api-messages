import {ApplicationService} from '@themost/common';
export declare class DefaultSmsService extends ApplicationService {
    sendSMS(context: any, message: any): Promise<any>;
    checkDelivery(message: any): Promise<any>;
    checkBalance(): Promise<any>;
}