/* eslint-disable no-unused-vars */
import {
	ApplicationService,
	Args,
	ArgumentError,
	DataError,
	HttpError,
	HttpServerError,
	IApplication,
	TraceUtils
} from '@themost/common';
import {DataContext} from '@themost/data';
const SMS = require('./models/sms-model');
import unirest from 'unirest';
import {SmsDeliveryStatusTypes} from './SmsDeliveryStatusTypes';
import { InvalidMessageRecipient } from './SmsService';
const os = require('os');

class DefaultSmsService extends ApplicationService {
	/**
     * @param {IApplication} app
     */
	constructor(app) {
		super(app);
		this.useCredentials = false;
		this.install(app);
	}

	/**
	 *
     * @param {IApplication} app
     */
	install(app) {
		if (app?.container) {
			let smsConfiguration;
			try {
				smsConfiguration = app.getConfiguration().getSourceAt('settings/universis/sms');
				if (!smsConfiguration) {
					throw new Error('SMSSettings are not defined');
				}
				Args.notNull(smsConfiguration?.serverUri, 'smsConfiguration.serverUri');
				Args.notNull(smsConfiguration?.endpoint, 'smsConfiguration.endpoint');
				Args.notNull(smsConfiguration?.balanceEndpoint, 'smsConfiguration.balanceEndpoint');
				Args.notNull(smsConfiguration?.deliveryEndpoint, 'smsConfiguration.deliveryEndpoint');
				this.settings = smsConfiguration;
				this.useCredentials = this.settings?.username && this.settings?.password;
			} catch (err) {
				throw new Error(`Error while loading SMS service settings. ${err}`);
			}
		} else {
			throw new Error('Application container not found. Make sure your api is up-to-date.');
		}
	}

	/**
	 * This uses the SMS provider API by creating a url based on the url and endpoint provided
	 * in application configuration, to send the message to the recipient.It checks if the
	 * recipient is a student or an instructor and selects the correct phone number based
	 * on the model.
     * @param {DataContext} context
     * @param {SMS} message
     */
	async sendSMS(context, message) {
		let recipient = message.recipient;
		const smsRecipient = await context.model('CandidateStudent')
			.where('user').equal(recipient)
			.select('person/mobilePhone as phone').silent()
			.getItem() ?? await context.model('Student')
			.where('user').equal(recipient)
			.select('person/mobilePhone as phone').silent()
			.getItem() ?? await context.model('Instructor')
			.where('user').equal(recipient)
			.select('workPhone as phone').silent()
			.getItem();
		if (smsRecipient == null) {
			throw new InvalidMessageRecipient('Message recipient cannot be found or is inaccessible'); 
		}
		if (smsRecipient.phone == null) {
			throw new InvalidMessageRecipient('Recipient phone cannot be found or is inaccessible');
		}
		let requestBody = !this.useCredentials ? {
			'from': this.settings.sender || this.settings.originator || 'UNIVERSIS',
			'to': smsRecipient?.phone,
			'message': message?.body,
			'local_dlr_url': (this?.settings?.apiHostName ?? os.hostname()) + `/SMS/${message.id}/status`,
			'application': 'UniverSIS'
		}: {
			from: this.settings.sender || this.settings.originator || 'UNIVERSIS',
			to: smsRecipient?.phone,
			username: this.settings.username,
			password: this.settings.password,
			dlr_url: (this?.settings?.apiHostName ?? os.hostname()) + `/SMS/${message.id}/status`,
			content: message?.body
		};
		return unirest.post(new URL(this?.settings?.endpoint, this?.settings?.serverUri))
			.header('Cache-Control', 'no-cache')
			.header('Accept', 'application/json')
			.send(requestBody)
			.end((response) => {
				if (response?.body?.status && response?.body?.status !== 200) {
					message.smsStatus = {'alternateName' : 'FailedSMSStatus'};
				}
				if (response?.error) {
					message.smsStatus = {'alternateName' : 'FailedSMSStatus'};
					if (response?.clientError) {
						context.model('SMS').silent().save(Object.assign(message, {'$state': 2})).then(() => {
							return new HttpError(response?.error?.status);
						});
					}
					context.model('SMS').silent().save(Object.assign(message, {'$state': 2})).then(() => {
						// return response?.error;
						return new HttpError(response.error);
					});
				}
				if (response?.body?.error) {
					message.smsStatus = {'alternateName' : 'FailedSMSStatus'};
					context.model('SMS').silent().save(Object.assign(message, {'$state': 2})).then(() => {
						return new Error('Error while trying to send sms');
					});
				}
				if (response?.body?.message) {
					const regex = /\b[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}\b/;
					// get identifier
					const identifier = regex.exec(response.body.message)[0];
					message.identifier = identifier;
					context.model('SMS').silent().save(Object.assign(message, {'$state': 2})).then((res) => {
						return res;
					});
				}
				return response?.body;
			});
	}

	/**
	 * This uses the SMS provider API by creating a url based on the url and endpoint provided
	 * in application configuration, to check for the left balance in the institute account.
	 * It posts to the endpoint of the SMS provider with and empty body and disables response
	 * caching and if the response status is 2xx it returns the response, else throws and error.
	 * @returns {Promise<*>}
	 */
	async checkBalance() {
		return new Promise((resolve,reject) => {
			unirest.post(new URL(this?.settings?.balanceEndpoint, this?.settings?.serverUri))
				.header('Cache-Control', 'no-cache')
				.end((response) => {
					if (response?.error) {
						return reject(new HttpError(response?.error));
					}
					else {
						if(response?.status >= 200 && response?.status<300) {
							return resolve(response?.response);
						}
						return reject(new HttpServerError('SMS_SRV',  'Status code and response don\'t match.'));
					}
				});
		});
	}

	/**
	 * This uses the SMS provider API by creating a url based on the url and endpoint provided
	 * in application configuration, to introspect the delivery status of the sms with the
	 * provider. This checks if the parameter is an SMS model or a GUID (SMS.identifier)
	 * and then posts to the delivery check endpoint the identifier.
	 * @param {SMS| string} message
	 * @throws ArgumentError
	 * @throws HttpServerError
	 */
	async checkDelivery(message) {
		let id = '';
		const re = /^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$/i;
		if (message instanceof SMS){
			id = message?.identifier;
		} else if (typeof message === 'string' && re.exec(message)) {
			id = message;
		} else {
			throw new ArgumentError('Invalid input. Expected an instance of SMS model or Guid.', 'E_ARG');
		}
		return new Promise((resolve, reject) => {
			unirest.post(new URL(this?.settings?.deliveryEndpoint, this?.settings?.serverUri))
				.header('Cache-Control', 'no-cache')
				.header('Accept', 'application/json')
				.send({'id': id})
				.end((response) => {
					if (response?.error) {
						return reject(response?.error);
					}
					if (Object.values(SmsDeliveryStatusTypes).includes(response?.response?.status)) {
						return resolve(response);
					} else {
						return reject(new HttpServerError('E_INVALID_RESPONSE', 'Response status not in valid sms delivery statuses.'));
					}
				});
		});
	}


}

export {
	DefaultSmsService
};
