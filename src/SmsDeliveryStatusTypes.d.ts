export declare const SmsDeliveryStatusTypes: SmsDeliveryStatusTypesEnum;

declare interface SmsDeliveryStatusTypesEnum {
    readonly DELIVRD: string;
    readonly ESME_ROK: string;
    readonly UNDELIV: string;
    readonly REJECTD: string;
}
