import {ApplicationService, DataError, HttpConflictError, HttpForbiddenError, TraceUtils} from '@themost/common';
import {Router} from 'express';
import {SmsDeliveryStatusTypes} from './SmsDeliveryStatusTypes';

class SmsRouterService extends ApplicationService {
	/**
	 *
	 * @param {IApplication} app
	 */
	constructor(app) {
		super(app);
		this.install(app);
	}

	/**
	 *
	 * @param {IApplication} app
	 */
	install(app){
		this.router = Router();
		this.whitelist = app.getConfiguration().getSourceAt('settings/universis/sms/whitelist') ?? [ '127.0.0.1' ];
		if (app?.container) {
			app?.container.subscribe(container => {
				if (container) {
					TraceUtils.log('Installing SMS Router service');
					this.router.use((req, res, next) => {
						// console.log("REQUEST TEST: "+ req);
						// // get remote address
						// const remoteAddress = req.headers['x-real-ip'] ?? req.headers['x-forwarded-for'] ?? req?.connection?.remoteAddress ?? req?.socket?.remoteAddress;
						// console.log("IP TEST: " + remoteAddress);
						// if (this.whitelist.indexOf(remoteAddress) < 0) {
						// 	return next(new HttpForbiddenError());
						// }
						return next();
					});
					// use this handler to create a router context
					this.router.use((req, res, next) => {
						// create router context
						const newContext = app.createContext();
						/**
						 * try to find if request has already a data context
						 * @type {ExpressDataContext|*}
						 */
						const interactiveContext = req.context;
						// finalize already assigned context
						if (interactiveContext) {
							if (typeof interactiveContext.finalize === 'function') {
								// finalize interactive context
								return interactiveContext.finalize(() => {
									// and assign new context
									Object.defineProperty(req, 'context', {
										enumerable: false,
										configurable: true,
										get: () => {
											return newContext;
										}
									});
									// exit handler
									return next();
								});
							}
						}
						// otherwise assign context
						Object.defineProperty(req, 'context', {
							enumerable: false,
							configurable: true,
							get: () => {
								return newContext;
							}
						});
						// and exit handler
						return next();
					});
					// use this handler to finalize router context
					// important note: context finalization is very important in order
					// to close and finalize database connections, cache connections etc.
					this.router.use((req, res, next) => {
						req.on('end', () => {
							//on end
							if (req.context) {
								//finalize data context
								return req.context.finalize( () => {
									//
								});
							}
						});
						return next();
					});
					this.router.post('/SMS/:id/status', async function (req, res, next) {
						let sms = await req.context.model('SMS').where('id').equal(req?.params?.id).silent().getTypedItem();
						if (sms) { 
							if(Object.keys(SmsDeliveryStatusTypes).includes(req.body?.status)){
								sms.smsStatus = {
									alternateName: SmsDeliveryStatusTypes[req.body.status]
								};
							} else {
								return next(new DataError(req.context.__('The SMS status isn\'t recognized. Try posting a valid SMS status')));
							}
							return res.json(await req.context.model('SMS').silent().save(sms));
						}
						return next(new HttpConflictError(req.context.__('There wasn\'t found an SMS with the specified id and identifier. Make sure the correct callback was passed') ));
					});
					container.use('/', this.router);
				}
			});
		} else {
			throw new Error('No Application Service Router is present');
		}
	}
}

module.exports = {SmsRouterService};
