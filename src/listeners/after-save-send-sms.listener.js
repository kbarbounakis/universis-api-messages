// eslint-disable-next-line no-unused-vars
import {DataEventArgs, DataContext} from '@themost/data';
import {SmsService} from '../SmsService';
import {AbstractMethodError} from '@themost/common';
import { InvalidMessageRecipient } from '../SmsService';

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
	if (event.state !== 1) {
		return callback();
	}
	// execute async method
	return AfterSaveSendSmsListener.afterSaveAsync(event).then(() => {
		return callback();
	}).catch(err => {
		return callback(err);
	});
}

export class AfterSaveSendSmsListener {
	/**
     * @param {DataEventArgs} event
     */
	static async afterSaveAsync(event) {
		if (event.state === 1) {
			let response;
			try {
				response = await this.sendMessage(event.target, event.model.context);
			} catch (error) {
				if (error instanceof InvalidMessageRecipient) {
					// set status and return
					Object.assign(event.target, {
						smsStatus: {
							alternateName: 'FailedSMSStatus'
						}
					});
					await event.model.save(event.target);
					return;
				}
				throw error;
			}
			if (response?.identifier) {
				Object.assign(event.target, {
					identifier: response.identifier,
					smsStatus: {
						alternateName: 'SentSMSStatus'
					}
				})
				return await event.model.save(event.target);
			}
		}
	}

	/**
	 *
	 * @param {SMS} message
	 * @param {DataContext} context
	 */
	static async sendMessage(message, context) {
		// regex for GUID inside response
		const re = /^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$/i;
		const sms = context.getApplication().getService(SmsService);
		try{
			const resp = await sms.sendSMS(context, message);
			if (resp?.status === 200 && !resp?.error && resp?.message) {
				let _message = re.exec(resp?.message);
				message.identifier = _message ?? message.identifier;
			}
			return message;
		} catch (e) {
			if (e instanceof InvalidMessageRecipient) {
				throw e;
			}
			if (e instanceof AbstractMethodError) {
				throw new Error('Make sure you use a service that implements the SmsService methods.');
			}
			throw new Error(`An error occurred while trying to send the SMS. Check the error for more details ${e}`);
		}
	}
}
