import {DataObject, EdmMapping, EdmType} from '@themost/data';
import {SmsService} from '../SmsService';
import {HttpError} from '@themost/common';

/**
 * @interface SMS
 *
 * @property {string} body
 * @property {number|Object} sender
 * @property {number|Object} recipient
 * @property {string} category
 * @property {string} identifier
 * @property {any} smsStatus
 */
@EdmMapping.entityType('SMS')
class SMS extends DataObject {

    @EdmMapping.action('checkDelivery', 'Object')
	async checkDelivery() {
		const smsService = this.context.getApplication().getService(SmsService);
		if (smsService && smsService?.checkDelivery && typeof smsService?.checkDelivery === 'function') {
			const sms = await this.context.model('SMS').where('id').equal(this.getId()).getTypedItem();
			return smsService.checkDelivery(sms);
		}
		return new HttpError(500, 'Method not implemented. Make sure the service has such a method and it is implemented.');
	}
}

module.exports = SMS;
